read -p "Are you sure you want to continue y/n?: "
if [ "$REPLY" == "y" ]
then
    echo "Logging off all users"
fi

if [ "$REPLY" == "n" ]
then
    echo "Continuing without logging off all users"
fi


# pass argument to an interactive prompt
# echo y | ./userpromptexample.sh