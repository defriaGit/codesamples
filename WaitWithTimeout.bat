ECHO OFF 
REM set timeout value in seconds
SET TIMEOUTVALUE=10 
SET TIMEOUT=%TIMEOUTVALUE%
SET FILEPATH="C:\Temp\some test file.txt"

:LOOP
SET /a TIMEOUT -= 1
ECHO "Checking response file"
IF EXIST %FILEPATH% (
    ECHO "BOOM!" 
    EXIT /b
)
TIMEOUT /t 1 /nobreak > NUL
IF %TIMEOUT% GTR 0 GOTO LOOP

ECHO "file check timed out after %TIMEOUTVALUE% seconds"
ECHO "expected file at %FILEPATH%, but was not found"
ECHO "BDA probably has not released a response file for sequence number %SEQUENCENUMBER%"