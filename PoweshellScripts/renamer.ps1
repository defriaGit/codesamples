param ([string]$CoursePath
)
Clear-Host

$Folders = Get-ChildItem -Path $CoursePath | Sort-Object -Descending -Property $_.FullName

foreach ($folder in $Folders){
    $childFiles = Get-ChildItem -Path $folder.FullName
    foreach ($child in $childFiles){
        $folderDigitPrefix = $folder.BaseName.Split(".")[0]
        $childOriginalName = $child.BaseName
        $exensiton = $child.Extension
        $childNewName = "$folderDigitPrefix." + $child.BaseName + $exensiton
        Rename-Item $child.FullName -NewName $childNewName
        Write-Host "$childOriginalName renamed to $childNewName" 
    }
}