namespace Refactor.Demo
{
    public interface IFileReader
    {
        string Read(string path);
    }
}