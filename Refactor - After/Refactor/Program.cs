﻿using Refactor.Demo;

namespace Refactor
{
    class Program
    {
        static void Main(string[] args)
        {
            var service = new VideoService(new FileReader());
            service.ReadVideoTitle();
        }
    }
}
