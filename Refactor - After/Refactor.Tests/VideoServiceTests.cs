using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using NUnit.Framework;
using Refactor.Demo;
using Assert = NUnit.Framework.Assert;

namespace Refactor.Tests
{
    [TestClass]
    public class VideoServiceTests
    {
        private Mock<IFileReader> _mockFileReader;
        private VideoService _videoService;

        [SetUp]
        public void SetUp()
        {
            _mockFileReader = new Mock<IFileReader>();
            _videoService = new VideoService(_mockFileReader.Object);
        }

        [Test]
        public void ReadVideoTitle_EmptyFile_ReturnErrorMessage()
        {
            // Arrange
            _mockFileReader.Setup(fr => fr.Read("video.txt")).Returns(string.Empty);


            // Act 
            var result = _videoService.ReadVideoTitle();

            // Assert
            Assert.That(result, Does.Contain("error").IgnoreCase);
        }
    }
}