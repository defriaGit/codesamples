﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using NUnit.Framework;
using Refactor.Demo.Utils;
using Refactor.Service;
using Assert = NUnit.Framework.Assert;

namespace Refactor.Tests.Services
{
    [TestFixture]
    public class VideoServiceTests
    {
        private string _jsonVideo;
        private Mock<IFileReader> _mockFileReader;
        private VideoService _videoService;

        [SetUp]
        public void Setup()
        {
            _mockFileReader = new Mock<IFileReader>();
            _videoService = new VideoService(_mockFileReader.Object);
            _jsonVideo = @"{'id': '1','title': 'abc','isProcessed': 'false' }";
            ;
        }

        [Test]
        public void ReadVideoTitle_WhenCalledWithEmptyString_ReturnErrorMessage()
        {
            // Arrange
            _mockFileReader.Setup(fr => fr.Read(It.IsAny<string>())).Returns(string.Empty);


            // Act
            var result = _videoService.ReadVideoTitle();

            // Assert
            Assert.That(result, Does.Contain("error").IgnoreCase);
        }

        [Test]
        public void ReadVideoTitle_WhenCalledWithJasonString_ReturnsTitle()
        {
            // Arrange
            _mockFileReader.Setup(fr => fr.Read(It.IsAny<string>())).Returns(_jsonVideo);

            // Act
            var result = _videoService.ReadVideoTitle();

            // Assert
            Assert.That(result.Equals("abc"));
        }
    }
}