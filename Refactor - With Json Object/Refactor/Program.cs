﻿using System;
using Refactor.Demo.Utils;
using Refactor.Service;

namespace Refactor
{
    class Program
    {
        static void Main(string[] args)
        {
           
            var service = new VideoService(new FileReader());
            var title = service.ReadVideoTitle();
            Console.WriteLine(title);
        }
    }
}
