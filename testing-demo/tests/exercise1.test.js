const lib = require('../exercise1');

describe('fizzbuzz', () => {
    it('should throw when value is not a number', () => {
        const args = ['abc',null,undefined,'',false,{}];
        args.forEach(a => {
            expect(() => {
                lib.fizzBuzz(a);
            }).toThrow();
        });
    });

    it('should return FizzBuzz when value is divisible by 3 and 5', () => {
        const result = lib.fizzBuzz(15);
        expect(result).toBe('FizzBuzz');
    });

    it('should return Fizz when value is divisible by 3 only', () => {
        const result = lib.fizzBuzz(3);
        expect(result).toBe('Fizz');
    });

    it('should return Buzz when value is divisible by 5 only', () => {
        const result = lib.fizzBuzz(5);
        expect(result).toBe('Buzz');
    });

    it('should return the value when value is  not divisible by 3 or 5', () => {
        const result = lib.fizzBuzz(1);
        expect(result).toEqual(1);
    });
});
